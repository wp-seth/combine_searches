usage:
  ./combine_searches.pl <search-url> (intersect|union|without|xor) <other_search-url>

licence: BSD-2-clause
