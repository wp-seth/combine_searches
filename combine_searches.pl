#!/usr/bin/env perl
use strict;
use warnings;
use open ':std', ':encoding(utf8)';
use Data::Dumper;
use JSON::XS;
use LWP::UserAgent;

sub api{
	my $url = shift;
	my $json = _api_single($url);
	my $results = $json->{'query'}{'search'};
	print "getting $json->{'query'}{'searchinfo'}{'totalhits'} results ...\n";
	while($json->{'continue'} && $json->{'continue'}{'sroffset'}){
		my $continue_url = $url . '&sroffset=' . $json->{'continue'}{'sroffset'};
		$json = _api_single($continue_url);
		push @$results, @{$json->{'query'}{'search'}};
	}
	return $results;
}

sub _api_single{
	my $url = shift;
	my $lwp = LWP::UserAgent->new();
	my $response = $lwp->get($url);
	my $json = decode_json $response->decoded_content;
	return $json;
}

sub intersect{
	my $A = shift;
	my $B = shift;
	my %titles_B;
	map {$titles_B{$_->{'title'}} = 1} @$B;
	return [grep {$titles_B{$_->{'title'}}} @$A];
}

sub set_without{
	my $A = shift;
	my $B = shift;
	my %titles_B;
	map {$titles_B{$_->{'title'}} = 1} @$B;
	return [grep {!$titles_B{$_->{'title'}}} @$A];
}

sub set_xor{
	my $A = shift;
	my $B = shift;
	my %titles;
	map {$titles{$_->{'title'}} = 1} @$B;
	map {++$titles{$_->{'title'}}} @$A;
	return [grep {$titles{$_->{'title'}} == 1} (@$A, @$B)];
}

sub union{
	my $A = shift;
	my $B = shift;
	my %titles;
	return [grep {++$titles{$_->{'title'}} == 1} (@$A, @$B)];
}

sub url2api{
	my $url = shift;
	my $api_url;
	if($url =~ /^https?:\/\/([a-z]+\.wikipedia\.org\/w\/)index.php\?(.*)\z/){
		$api_url = 'https://' . $1 . 'api.php?action=query&list=search&format=json&srlimit=max&srprop=timestamp|snippet';
		my %params = (map {split /=/, $_, 2} split /&/, $2);
		my @keys = keys %params;
		my @ns = ();
		for my $key(@keys){
			if(grep {$key eq $_} ('title', 'fulltext', 'profile')){
				next;
			}
			if($key eq 'search'){
				$api_url .= '&srsearch=' . $params{'search'};
			}elsif($key =~ /^ns([0-9]+)\z/){
				push @ns, $1;
			}else{
				print "warning: unused param '$key'. maybe the script has to be adapted.";
			}
		}
		$api_url .= '&srnamespace=' . join('|', sort {$a <=> $b} @ns) if @ns > 0;
	}
	return $api_url;
}

my $results = [];
if(@ARGV == 1 && $ARGV[0] =~ /^http/){
	$results = api(url2api($ARGV[0]));
}elsif(@ARGV == 3 && $ARGV[1] =~ /^(?:intersect|union|without|xor)\z/){
	my $results_A = api(url2api($ARGV[0]));
	my $results_B = api(url2api($ARGV[2]));
	if($ARGV[1] eq 'intersect'){
		$results = intersect($results_A, $results_B);
	}elsif($ARGV[1] eq 'union'){
		$results = union($results_A, $results_B);
	}elsif($ARGV[1] eq 'without'){
		$results = set_without($results_A, $results_B);
	}elsif($ARGV[1] eq 'xor'){
		$results = set_xor($results_A, $results_B);
	}
}else{
	print "usage:\n";
	print "  $0 <search-url>\n";
	print "  $0 <search-url> (intersect|union|without|xor) <other_search-url>\n\n";
	print "example:\n";
	print "  $0 'https://de.wikipedia.org/w/index.php?title=Special:Search&...' " 
		. "intersect 'https://de.wikipedia.org/w/index.php?title=Special:Search&...'\n";
	exit 1;
}

print "result: " . scalar(@$results). " elements\n";
print "{| class=\"wikitable sortable\"\n";
print "! title\n";
print "! timestamp\n";
print "! snippet\n";
for my $r(@$results){
	$r->{'snippet'} =~ s/<span class="searchmatch">(.*?)<\/span>/<\/nowiki>'''<nowiki>$1<\/nowiki>'''<nowiki>/g;
	print "|-\n";
	print "| [[$r->{'title'}]]\n";
	print "| $r->{'timestamp'}\n";
	print "| <nowiki>$r->{'snippet'}</nowiki>\n";
}
print "|}\n";

